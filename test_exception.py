import asyncio
import logging


async def problem() -> None:
    await asyncio.sleep(1)
    logging.warning('Going to raise an exception now!')
    raise RuntimeError('Something went wrong')


if __name__ == '__main__':
    logging.basicConfig(
        format = '▸ %(asctime)s.%(msecs)03d %(filename)s:%(lineno)d %(levelname)s %(message)s',
        level = logging.INFO,
        datefmt = '%H:%M:%S',
    )
    loop = asyncio.get_event_loop()
    logging.info('Creating the problem task')
    task = loop.create_task(problem())
    logging.info('Created task = %r', task)
    logging.info('Running the loop')
    try:
        loop.run_forever()
    except KeyboardInterrupt:
        logging.info('Closing the loop')
        loop.close()
    logging.info('Shutting down')