import asyncio
import time


async def run():
    start = time.time()
    print(f"run at {start}")
    await asyncio.sleep(1)
    end = time.time()
    print(f"stop at {end}. Took {end - start}")


async def inf():
    print('start')
    for i in range(100000000):
        pass
    print('end')

async def main():
    await asyncio.gather(run(), inf())  


if __name__ == '__main__':
    asyncio.run(main())
