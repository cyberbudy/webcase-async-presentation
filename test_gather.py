import asyncio


async def run():
    for i in range(10):
        print(i)
        await asyncio.sleep(1)        


async def inf():
    print('start')
    for i in range(100000000):
        pass
    print('end')


async def main():
   await asyncio.gather(run(), inf())


if __name__ == '__main__':
    asyncio.run(main())
