import asyncio


async def first():
    for i in range(6):
        print('first: ', i)
        await asyncio.sleep(0.1)
        if i == 1:
            raise ValueError()
    return 1


async def second():
    for i in range(6):
        print('second: ', i)
        await asyncio.sleep(0.1)

        if i == 3:
            raise ValueError()        
    return 2


async def third():
    for i in range(6):
        print('third: ', i)
        await asyncio.sleep(0.1)

    return 3


async def main():
    print('result: ', await asyncio.gather(first(), second(), third(), return_exceptions=False))

    print(123123)


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    loop.create_task(main())

    # try:
    loop.run_forever()
    # except KeyboardInterrupt:
    #     loop.close()
